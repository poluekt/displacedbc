# Generate prompt Bc 
python gen_bc.py

# Generate Bc from bbud decay with different masses and lifetimes
python gen.py hist_tau02_m10500.root 10.5 0.2
python gen.py hist_tau05_m10500.root 10.5 0.5
python gen.py hist_tau10_m10500.root 10.5 1.0
python gen.py hist_tau02_m9500.root 9.5 0.2
python gen.py hist_tau05_m9500.root 9.5 0.5
python gen.py hist_tau10_m9500.root 9.5 1.0
