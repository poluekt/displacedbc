from ROOT import TCanvas, TFile, gROOT, TLegend, gStyle, TF1
from math import exp

gROOT.ProcessLine(".x lhcbstyle2.C")
gStyle.SetLegendTextSize(0.055)
gStyle.SetLegendFont(132)

f0 = TFile.Open("hist_ip_d0.root")
f0.cd()
h0 = gROOT.FindObject("h2")
h0.SetDirectory(0)
f0.Close()

h0.SetLineStyle(1)
h0.SetLineColor(2)

#h0.Scale( h6.GetMaximum() / h0.GetMaximum() )

c = TCanvas("c1", "", 600, 600)
h0.Draw()
h0.GetXaxis().SetTitle("ln (IP/mm)")

def bifgaus(x, q) :
  m = q[1]
  sigmal1 = q[2]
  sigmal2 = q[3]
  rsigma = q[4]
  sigma1 = sigmal1
  sigma2 = sigmal2
  if x[0] > m : 
    sigma1 = sigmal1/rsigma
    sigma2 = sigmal2/rsigma
  n1 = q[0]
  n2 = q[0]*q[5]
  if sigma1 != 0 :
    result = n1*exp(-0.5*(x[0]-m)*(x[0]-m)/sigma1/sigma1)/sigmal1
  else :
    result = 0.
  if sigma2 != 0 :
    result += n2*exp(-0.5*(x[0]-m)*(x[0]-m)/sigma2/sigma2)/sigmal2
  return result

fbifgaus = TF1("bifgaus",bifgaus,-7., 2., 6)
fbifgaus.SetParameters(1000., -3.2, 0.7, 1.3, 1.5, 0.7)
#h0.Fit(fbifgaus)

fbifgaus0 = TF1("bifgaus",bifgaus,-7., 2., 6)
fbifgaus0.SetParameters(3200., -3.247, 0.685, 1.29, 1.34, 0.733)
fbifgaus0.SetLineStyle(7)
fbifgaus0.Draw("same")

l = TLegend(0.55, 0.70, 0.89, 0.9)
l.SetHeader("Prompt D^{0}#rightarrow K#pi")
l.AddEntry(h0, "toy MC", "l")
l.AddEntry(fbifgaus0, "fitted data", "l")
l.Draw("e")

c.Print("plot_d0.eps")
