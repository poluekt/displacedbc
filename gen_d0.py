#
# Generate prompt D0 -> K pi decays for validation purpose
#

from ROOT import TRandom3, TMath, TGenPhaseSpace, TLorentzVector, TVector3, TH1F, TFile
import array

def generate_lt(rnd, tau) : 
  lt = -tau*TMath.Log(rnd.Rndm()) # Proper lifetime 
  return lt

def generate_pt(rnd) : 
  pt = -1.5*TMath.Log(rnd.Rndm()) # Generate pT with slope parameter 1.5 GeV 
                                  # (more or less as follows from arXiv:1302.2864)
  return pt

def generate_eta(rnd) : 
  eta = 1.5 + 4.*rnd.Rndm()      # Eta, uniform in (1.5, 5.5)
  return eta

def generate_phi(rnd) : 
  phi = 2.*TMath.Pi()*rnd.Rndm() # Phi, uniform in (0, 2pi)
  return phi

def impact_param(x, v) : 
  xv = x.Dot(v)
  v2 = v.Mag2()
  t = -xv/v2
  nearest = x + v*t
  ip = nearest.Mag()
  return ip

def ipres(pt) : 
  return (13.2 + 24.7/pt)*1.2  # Track IP resolution for Run1 (2011) in um, as a function of track Pt [GeV]

def main() : 
  rnd = TRandom3()

  nev = 1000000

  kptcut  = 0.3
  kpcut   = 3.
  piptcut  = 0.3
  pipcut   = 3.
  etamin = 2.
  etamax = 5.
  sumptmin = 1.4

  c = 29.9792458e-3 # c in cm/ns
  m = 1.864

  masses_kpi = [ 0.497, 0.139 ]        # D0 decays to K pi

  amasses_kpi = array.array('d', masses_kpi)

  h1 = TH1F("h1", "IP", 300, -7., 2.)
  h2 = TH1F("h2", "IP", 300, -7., 2.)

  h_pi_ip = TH1F("piip", "IP(pi)", 300, 0., 1.5)
  h_k_ip = TH1F("kip", "IP(K)", 300, 0., 1.5)
  h_d_ip = TH1F("dip", "IP(D)", 300, 0., 0.15)

  h_pi_pt = TH1F("pipt", "Pt(pi)", 300, 0., 10.)
  h_k_pt = TH1F("kpt", "Pt(K)", 300, 0., 10.)

  h_pi_p = TH1F("pip", "P(pi)", 300, 0., 100.)
  h_k_p = TH1F("kp", "P(K)", 300, 0., 100.)

  h_d_p = TH1F("dp", "P(D)", 300, 0., 150.)
  h_d_pt = TH1F("dpt", "Pt(D", 300, 0., 10.)

  for i in range(nev) : 

    pt  = generate_pt(rnd)      # Pt in GeV
    eta = generate_eta(rnd)     # Eta
    phi = generate_phi(rnd)     # Phi

    theta = 2.*TMath.ATan(TMath.Exp(-eta))
    p  = pt/TMath.Sin(theta)     # Full momentum
    e  = TMath.Sqrt(p**2 + m**2) # Energy
    px = p*TMath.Sin(theta)*TMath.Sin(phi)
    py = p*TMath.Sin(theta)*TMath.Cos(phi)
    pz = p*TMath.Cos(theta)
    gamma = TMath.Sqrt(1 + p**2/m**2)

    p4 = TLorentzVector(px, py, pz, e)  # 4-momentum of D0

    decay2 = TGenPhaseSpace()
    decay2.SetDecay(p4, 2, amasses_kpi)
    decay2.Generate()
    pk4  = decay2.GetDecay(0)
    ppi4 = decay2.GetDecay(1)

    sel = 1
    if pk4.P()   < kpcut   : sel = 0
    if pk4.Pt()  < kptcut  : sel = 0
    if pk4.Eta() < etamin  : sel = 0
    if pk4.Eta() > etamax  : sel = 0
    if ppi4.P()    < pipcut  : sel = 0
    if ppi4.Pt()   < piptcut : sel = 0
    if ppi4.Eta()  < etamin  : sel = 0
    if ppi4.Eta()  > etamax  : sel = 0
    if pk4.Pt() + ppi4.Pt() < sumptmin : sel = 0

    pi_ipres = ipres(ppi4.Pt())
    k_ipres  = ipres(pk4.Pt())

#    d_ipres = 1./TMath.Sqrt( 1./(pi_ipres**2) + 1./(k_ipres**2) )
#    ipx = rnd.Gaus()*d_ipres
#    ipy = rnd.Gaus()*d_ipres

#    d_ip = TMath.Sqrt( ipx**2 + ipy**2 )

    pi_ipx = rnd.Gaus()*pi_ipres
    pi_ipy = rnd.Gaus()*pi_ipres
    k_ipx = rnd.Gaus()*k_ipres
    k_ipy = rnd.Gaus()*k_ipres

    d_ipx = (pi_ipx/pi_ipres**2 + k_ipx/k_ipres**2)/(1./pi_ipres**2 + 1./k_ipres**2)
    d_ipy = (pi_ipy/pi_ipres**2 + k_ipy/k_ipres**2)/(1./pi_ipres**2 + 1./k_ipres**2)
    d_ip = TMath.Sqrt( d_ipx**2 + d_ipy**2 )

    pi_ip = TMath.Sqrt( pi_ipx**2 + pi_ipy**2 )
    k_ip = TMath.Sqrt( k_ipx**2 + k_ipy**2 )

    log_ip = TMath.Log( d_ip/1000. )

#    print pbc4.X(), pbc4.Y(), pbc4.Z(), pbc4.M(), ip, sel
    h1.Fill(log_ip)
    if sel : 
      h2.Fill(log_ip)
      h_d_ip.Fill(d_ip/1000.)
      h_pi_ip.Fill(pi_ip / 1000.)
      h_k_ip.Fill(k_ip / 1000.)
      
      h_pi_pt.Fill(ppi4.Pt())
      h_k_pt.Fill(pk4.Pt())
      h_pi_p.Fill(ppi4.P())
      h_k_p.Fill(pk4.P())
      h_d_p.Fill(p4.P())
      h_d_pt.Fill(p4.Pt())

#    print lt, pt, eta, phi, gamma, dist, vz

  f = TFile.Open("hist_ip_d0.root", "RECREATE")
  h1.Write()
  h2.Write()
  h_pi_ip.Write()
  h_k_ip.Write()
  h_pi_pt.Write()
  h_k_pt.Write()
  h_pi_p.Write()
  h_k_p.Write()
  h_d_p.Write()
  h_d_pt.Write()
  h_d_ip.Write()
  f.Close()

if __name__ == "__main__" : 
  main()
