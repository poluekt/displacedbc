#
# Generate bbud -> Bc D K, Bc -> Jpsi pi decays
#

from ROOT import TRandom3, TMath, TGenPhaseSpace, TLorentzVector, TVector3, TH1F, TFile, TH2F
import array
import sys

# Generate proper lifetime as exponential distribution with mean "tau"
def generate_lt(rnd, tau) : 
  lt = -tau*TMath.Log(rnd.Rndm()) # Proper lifetime 
  return lt

# Generate Pt distribution (exponential with mean of 5 GeV)
def generate_pt(rnd) : 
  pt = -5.*TMath.Log(rnd.Rndm())
  return pt

# Generate eta (flat in 1.5, 5.5)
def generate_eta(rnd) : 
  eta = 1.5 + 4.*rnd.Rndm()      # Eta, uniform in (1.5, 5.5)
  return eta

# Generate phi (flat from 0 to 2pi)
def generate_phi(rnd) : 
  phi = 2.*TMath.Pi()*rnd.Rndm() # Phi, uniform in (0, 2pi)
  return phi

# Calculate IP for a particle produced at 3D point x and direction vector v
def impact_param(x, v) : 
  xv = x.Dot(v)
  v2 = v.Mag2()
  t = -xv/v2
  nearest = x + v*t
  ip = nearest.Mag()
  return ip

# Parametrisation of IP resolution as a function of Pt
def ipres(pt) : 
  return 11.6 + 23.4/pt  # Track IP resolution for Run1 (2012) in um, as a function of track Pt [GeV]

def main() : 
  rnd = TRandom3()

  nev = 1000000
  m   = 9.5        # bbud mass in GeV
  tau = 1.0        # bbud lifetime in ps
  outfile = "hist_tau10_m9500.root"

  if len(sys.argv)>1 : outfile = sys.argv[1]
  if len(sys.argv)>2 : m   = float(sys.argv[2])
  if len(sys.argv)>3 : tau = float(sys.argv[3])

  # Cuts for daughter muons and pions
  muptcut  = 0.55
  mupcut   = 3.
  piptcut  = 1.0
  pipcut   = 3.
  etamin = 2.
  etamax = 5.

  c = 29.9792458e-3 # speed of light in cm/ns
  masses_bdk    = [ 6.274, 1.869, 0.497 ] # bbud decays into Bc D+ K-
  masses_jpsipi = [ 3.097, 0.139 ]        # Bc decays to Jpsi pi
  masses_mumu   = [ 0.106, 0.106 ]        # Jpsi decays to mu mu

  amasses_bdk    = array.array('d', masses_bdk)
  amasses_jpsipi = array.array('d', masses_jpsipi)
  amasses_mumu   = array.array('d', masses_mumu)

  h1 = TH1F("h1", "No cuts",   300, -7., 2)
  h2 = TH1F("h2", "With cuts", 300, -7., 2)
  h3 = TH1F("h3", "Smeared",   300, -7., 2)
  hipp  = TH2F("hipp",  "IP vs P",  100, -7., 2., 100,  2., 9.)
  hippt = TH2F("hippt", "IP vs Pt", 100, -7., 2., 100, -1., 5.)

  for i in range(nev) : 
    lt  = generate_lt(rnd, tau) # Proper lifetime in ps
    pt  = generate_pt(rnd)      # Pt in GeV
    eta = generate_eta(rnd)     # Eta
    phi = generate_phi(rnd)     # Phi

    # parameters of the generated bbud state
    theta = 2.*TMath.ATan(TMath.Exp(-eta))  # theta in radians
    p  = pt/TMath.Sin(theta)                # Full momentum
    e  = TMath.Sqrt(p**2 + m**2)            # Energy
    px = p*TMath.Sin(theta)*TMath.Sin(phi)
    py = p*TMath.Sin(theta)*TMath.Cos(phi)
    pz = p*TMath.Cos(theta)
    gamma = TMath.Sqrt(1 + p**2/m**2)

    dist = lt*c*gamma                         # bbud decay distance
    vx = dist*TMath.Sin(theta)*TMath.Sin(phi) # bbud decay vertex coordinates
    vy = dist*TMath.Sin(theta)*TMath.Cos(phi)
    vz = dist*TMath.Cos(theta)

    p4 = TLorentzVector(px, py, pz, e)  # 4-momentum of bbud
    decay1 = TGenPhaseSpace()
    decay1.SetDecay(p4, 3, amasses_bdk)
    decay1.Generate()                   # Generate bbud -> Bc D+ K- phase space decay
    pbc4   = decay1.GetDecay(0)         # Get 4-momentum of Bc

    decay2 = TGenPhaseSpace()
    decay2.SetDecay(pbc4, 2, amasses_jpsipi)
    decay2.Generate()                   # Generate Bc -> Jpsi pi decay
    pjpsi4 = decay2.GetDecay(0)
    ppi4   = decay2.GetDecay(1)

    decay3 = TGenPhaseSpace()
    decay3.SetDecay(pjpsi4, 2, amasses_mumu)
    decay3.Generate()                   # Generate Jpsi -> mu mu decay
    pmup4  = decay3.GetDecay(0)
    pmum4  = decay3.GetDecay(1)

    # Select only events passing cuts
    sel = 1
    if pmup4.P()   < mupcut  : sel = 0
    if pmup4.Pt()  < muptcut : sel = 0
    if pmup4.Eta() < etamin  : sel = 0
    if pmup4.Eta() > etamax  : sel = 0
    if pmum4.P()   < mupcut  : sel = 0
    if pmum4.Pt()  < muptcut : sel = 0
    if pmum4.Eta() < etamin  : sel = 0
    if pmum4.Eta() > etamax  : sel = 0
    if ppi4.P()    < pipcut  : sel = 0
    if ppi4.Pt()   < piptcut : sel = 0
    if ppi4.Eta()  < etamin  : sel = 0
    if ppi4.Eta()  > etamax  : sel = 0

    ip = impact_param( TVector3(vx, vy, vz), pbc4.Vect() ) # impact parameter of Bc

    # IP resolutions for each of Bc daughters
    ipres1 = ipres(ppi4.Pt())
    ipres2 = ipres(pmum4.Pt())
    ipres3 = ipres(pmup4.Pt())

    # Weighted resolution for Bc IP resolution
    b_ipres = 1./TMath.Sqrt( 1./(ipres1**2) + 1./(ipres2**2) + 1./(ipres3**2) )
    b_ipres = b_ipres/1e4 # Transform um to cm

    # Smear IP with resolution
    ip2 = TMath.Sqrt( (ip + rnd.Gaus()*b_ipres)**2 + (rnd.Gaus()*b_ipres)**2 )

    ip = TMath.Log( ip*10. )
    ip2 = TMath.Log( ip2*10. )

#    print pbc4.X(), pbc4.Y(), pbc4.Z(), pbc4.M(), ip, sel

    # Store everything to histograms
    h1.Fill(ip)
    if sel : 
      h2.Fill(ip)
      h3.Fill(ip2)
      hipp.Fill(ip2, TMath.Log(pbc4.P()))
      hippt.Fill(ip2, TMath.Log(pbc4.Pt()))

#    print lt, pt, eta, phi, gamma, dist, vz

  f = TFile.Open(outfile, "RECREATE")
  h1.Write()
  h2.Write()
  h3.Write()
  hipp.Write()
  hippt.Write()
  f.Close()

if __name__ == "__main__" : 
  main()

