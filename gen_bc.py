#
# Generate prompt Bc -> Jpsi pi decays
#

from ROOT import TRandom3, TMath, TGenPhaseSpace, TLorentzVector, TVector3, TH1F, TFile, TH2F
import array

def generate_lt(rnd, tau) : 
  lt = -tau*TMath.Log(rnd.Rndm()) # Proper lifetime 
  return lt

def generate_pt(rnd) : 
  pt = -5.*TMath.Log(rnd.Rndm())
  return pt

def generate_eta(rnd) : 
  eta = 1.5 + 4.*rnd.Rndm()      # Eta, uniform in (1.5, 5.5)
  return eta

def generate_phi(rnd) : 
  phi = 2.*TMath.Pi()*rnd.Rndm() # Phi, uniform in (0, 2pi)
  return phi

def impact_param(x, v) : 
  xv = x.Dot(v)
  v2 = v.Mag2()
  t = -xv/v2
  nearest = x + v*t
  ip = nearest.Mag()
  return ip

def ipres(pt) : 
  return 11.6 + 23.4/pt  # Track IP resolution for Run1 (2012) in um, as a function of track Pt [GeV]

def main() : 
  rnd = TRandom3()

  nev = 1000000

  muptcut  = 0.55
  mupcut   = 3.
  piptcut  = 1.0
  pipcut   = 3.
  etamin = 2.
  etamax = 5.

  c = 29.9792458e-3 # c in cm/ns
  m = 6.274

  masses_jpsipi = [ 3.097, 0.139 ]        # Bc decays to Jpsi pi
  masses_mumu   = [ 0.106, 0.106 ]        # Jpsi decays to mu mu

  amasses_jpsipi = array.array('d', masses_jpsipi)
  amasses_mumu   = array.array('d', masses_mumu)

  h1 = TH1F("h1", "IP", 300, -7., 2.)
  h2 = TH1F("h2", "IP", 300, -7., 2.)
  hipp  = TH2F("hipp",  "IP vs P",  100, -7., 2., 100,  2., 9.)
  hippt = TH2F("hippt", "IP vs Pt", 100, -7., 2., 100, -1., 5.)

  for i in range(nev) : 

    pt  = generate_pt(rnd)      # Pt in GeV
    eta = generate_eta(rnd)     # Eta
    phi = generate_phi(rnd)     # Phi

    theta = 2.*TMath.ATan(TMath.Exp(-eta))
    p  = pt/TMath.Sin(theta)     # Full momentum
    e  = TMath.Sqrt(p**2 + m**2) # Energy
    px = p*TMath.Sin(theta)*TMath.Sin(phi)
    py = p*TMath.Sin(theta)*TMath.Cos(phi)
    pz = p*TMath.Cos(theta)
    gamma = TMath.Sqrt(1 + p**2/m**2)

    p4 = TLorentzVector(px, py, pz, e)  # 4-momentum of bbud

    decay2 = TGenPhaseSpace()
    decay2.SetDecay(p4, 2, amasses_jpsipi)
    decay2.Generate()
    pjpsi4 = decay2.GetDecay(0)
    ppi4   = decay2.GetDecay(1)

    decay3 = TGenPhaseSpace()
    decay3.SetDecay(pjpsi4, 2, amasses_mumu)
    decay3.Generate()
    pmup4  = decay3.GetDecay(0)
    pmum4  = decay3.GetDecay(1)

    sel = 1
    if pmup4.P()   < mupcut  : sel = 0
    if pmup4.Pt()  < muptcut : sel = 0
    if pmup4.Eta() < etamin  : sel = 0
    if pmup4.Eta() > etamax  : sel = 0
    if pmum4.P()   < mupcut  : sel = 0
    if pmum4.Pt()  < muptcut : sel = 0
    if pmum4.Eta() < etamin  : sel = 0
    if pmum4.Eta() > etamax  : sel = 0
    if ppi4.P()    < pipcut  : sel = 0
    if ppi4.Pt()   < piptcut : sel = 0
    if ppi4.Eta()  < etamin  : sel = 0
    if ppi4.Eta()  > etamax  : sel = 0

    ipres1 = ipres(ppi4.Pt())
    ipres2 = ipres(pmum4.Pt())
    ipres3 = ipres(pmup4.Pt())

    b_ipres = 1./TMath.Sqrt( 1./(ipres1**2) + 1./(ipres2**2) + 1./(ipres3**2) )
    ipx = rnd.Gaus()*b_ipres
    ipy = rnd.Gaus()*b_ipres
    ip = TMath.Sqrt( ipx**2 + ipy**2 )

    ip = TMath.Log( ip/1000. )

#    print pbc4.X(), pbc4.Y(), pbc4.Z(), pbc4.M(), ip, sel
    h1.Fill(ip)
    if sel : 
      h2.Fill(ip)
      hipp.Fill(ip, TMath.Log(p4.P()))
      hippt.Fill(ip, TMath.Log(p4.Pt()))

#    print lt, pt, eta, phi, gamma, dist, vz

  f = TFile.Open("hist_ip_bc.root", "RECREATE")
  h1.Write()
  h2.Write()
  hipp.Write()
  hippt.Write()
  f.Close()

if __name__ == "__main__" : 
  main()
